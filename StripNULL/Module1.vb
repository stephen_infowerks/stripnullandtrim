﻿Imports System.IO
Imports System.Text

Module Module1

    Sub Main()

        Dim args() As String = Environment.GetCommandLineArgs
        Dim argc As Integer = args.GetUpperBound(0)
        Dim fnIN, fnOUT, ssIN, ssOUT As String
        Dim delim As String

        delim = "|"

        If argc >= 1 Then
            fnIN = Trim(args(1))
        Else
            Console.WriteLine("Usage:  stripnull input-file [output-file]")
            Console.WriteLine("No input file specified - ABORTING!")
            Environment.Exit(100)
        End If

        If Not File.Exists(fnIN) Then
            Console.WriteLine("Input file - does not exist")
            Environment.Exit(100)
        End If

        If argc >= 2 Then
            fnOUT = Trim(args(2))
        Else
            Dim tfn As String
            Dim perPOS, slhPOS As Integer
            perPOS = InStrRev(fnIN, ".")
            slhPOS = InStrRev(fnIN, "\")
            If slhPOS > 0 Then  'there's a back-slash in there.
                If perPOS > slhPOS Then
                    tfn = Left(fnIN, perPOS).Trim & ".out"
                Else
                    tfn = fnIN.Trim & ".out"
                End If
            Else
                If perPOS > 0 Then
                    tfn = Left(fnIN, perPOS).Trim & ".out"
                Else
                    tfn = fnIN.Trim & ".out"
                End If
            End If
            fnOUT = tfn.Trim
        End If

        If argc >= 3 Then
            delim = Trim(args(3))
        End If
        If IsNumeric(delim) Then
            delim = Convert.ToString(Convert.ToChar(Convert.ToByte(Val(delim))))
        End If
        
        'fnIN = "f:\temp\medicap\refill.rpt"
        'fnOUT = "f:\temp\medicap\refill.OUT"

        If File.Exists(fnOUT) Then
            File.Delete(fnOUT)
        End If

        If File.Exists(fnOUT) Then
            Console.WriteLine("OUTput file - exists - delete/createnew failed!")
            Environment.Exit(100)
        End If

        If File.Exists(fnIN) Then
            Dim strIN As New StreamReader(fnIN, Encoding.GetEncoding("iso-8859-1"))
            Dim strOUT As New StreamWriter(fnOUT, FileMode.Create, Encoding.GetEncoding("iso-8859-1"))
            Dim s As String()
            Dim t As String
            Do While strIN.Peek() >= 0
                ssIN = strIN.ReadLine
                ssOUT = ""
                s = ssIN.Split(delim)
                For i = 0 To s.Length - 1
                    t = s(i).Trim
                    If StrComp(t, "NULL") = 0 Then t = ""
                    s(i) = t & delim
                Next
                ssOUT = String.Concat(s)
                strOUT.WriteLine(Left(ssOUT, ssOUT.Length - 1))
            Loop
            strIN.Close()
            strOUT.Close()
        End If
    End Sub

End Module
